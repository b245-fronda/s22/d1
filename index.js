// console.log("Hello world");

// Array methods
	// JavaScript has built-in functions and methods for arrays. This allows us to manipulate and access array elements.

// [Section] Mutator Methods
	// Mutator methods are functions that mutate or change an array after they've created.
	// these methods manipulate the original array performing various tasks as adding and removing elements.

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
console.log(fruits);

	// First Mutator method: push()
		// Adds an element/s in the end of an array and returns the array's length
		// Syntax:
			// arrayName.push(element/sToBeAdded);

console.log("Current Array fruits []:");
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log("Mutated array after the push method:");
console.log(fruits);
console.log(fruitsLength);

// Adding multiple elements to an array
fruitsLength = fruits.push("Avocado", "Guava");
console.log("Mutated array from the push method");
console.log(fruits);
console.log(fruitsLength);

	// Second Mutator method: pop()
		// Removes the last element in an array and returns the removed element
		// Syntax:
			// arrayName.pop();

console.log("Current Array fruits []:");
console.log(fruits);

let removedFruit = fruits.pop();
console.log("Mutated array after the pop method:");
console.log(fruits);
console.log(removedFruit);

	// Third Mutator method: unshift()
		// it adds one or more elements at the beginning of an array and returns the present length.
		// Syntax:
			// arrayName.unshift(element/sToBeAdded);

console.log("Current Array fruits []:");
console.log(fruits);

fruitsLength = fruits.unshift('Lime', 'Banana');
console.log("Mutated array after the unshift method:");
console.log(fruits);
console.log(fruitsLength);

	// Fourth Mutator method: shift()
		// removes an element at the beginning of an array and return the removed element.

console.log("Current Array fruits []:");
console.log(fruits);

removedFruit = fruits.shift();
console.log("Mutated array after the shift method:");
console.log(fruits);
console.log(removedFruit);

	// Fifth Mutator method: splice()
		// simultaneously removes elements from a specified index number and adds elements.
	// Syntax:
		// arrayName.splice(startingIndex, deleteCount, element/sToBeAdded);

console.log("Current Array fruits []:");
console.log(fruits);

let splice = fruits.splice(1, 2, "Lime", "Guava");
console.log("Mutated array after the splice method:");
console.log(fruits);
console.log(splice);

console.log("Current Array fruits []:");
console.log(fruits);
fruits.splice(1, 2);
console.log(fruits);

console.log("Current Array fruits []:");
console.log(fruits);
fruits.splice(2, 0, "Durian", "Santol");
console.log(fruits);

	// Sixth Mutator method: sort()
		// rearrange the array elements in alphanumeric order
		// Syntax:
			// arrayName.sort();

console.log("Current Array fruits []:");
console.log(fruits);

fruits.sort();
console.log("Mutated array after the sort method:");
console.log(fruits);

	// Seventh Mutator method: reverse()
		// reverses the order of array elements
		// Syntax: arrayName.reverse();

console.log("Current Array fruits []:");
console.log(fruits);

fruits.reverse();
console.log("Mutated array after the reverse method:");
console.log(fruits);

// [SECTION] Non-mutator Methods

	// Non-mutator methods are functions that do not modify or change an array after they're created.

	// these methods do not manipulate the original array performing task such as returning elements from an array and combining arrays and printing the output.

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

		// First non-mutator method: indexOf()
			// it returns the index number of the first matching element found in an array
			// if no match was found, the result will be -1.
			// the search process will be done from first element proceeding to the last element.
			// Syntax: arrayName.indexOf(searchValue);

	console.log(countries);
	console.log(countries.indexOf('PH'));
	console.log(countries.indexOf('BR'));

			// in indexOf() we can set the starting index

	console.log(countries.indexOf('PH', 2));

		// Second non-mutator method: lastIndexOf()
			// it returns the index number of the last matching element found in an array.

	console.log(countries.lastIndexOf('PH'));

		// Third non-mutator method: slice()
			// portion/slices from array and returns new array
			// Syntax: arrayName.slice(startingIndex);
					// arrayName.slice(startingIndex, endingIndex);

	let slicedArrayA = countries.slice(2);
	console.log(slicedArrayA);
	console.log(countries);

	let slicedArrayB = countries.slice(1, 5);
	console.log(slicedArrayB);

		// Fourth non-mutator method: toString();
			// return an array as string separated by comma
			// Syntax: arrayName.toString();

	let stringArray = countries.toString();
	console.log(stringArray);
	console.log(stringArray.length);

		// Fifth non-mutator method: concat()
			// combines arrays and returns the combined result
			// Syntax: arrayA.concat(arrayB);
					// arrayA.concat(elementA);

	let tasksArrayA = ["drink HTML", "eat javascript"];
	let tasksArrayB = ['inhale CSS', 'breathe SASS'];
	let tasksArrayC = ['get git', 'be node'];

	let tasks = tasksArrayA.concat(tasksArrayB);
	console.log(tasks);

	// adding/combining multiple arrays
	let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
	console.log(allTasks);

	// combine arrays with elements
	let combinedTasks = tasksArrayA.concat(tasksArrayB, "smell express", "throw react");
	console.log(combinedTasks);

		// Sixth non-mutator method: join()
			// returns an array as string separated by specified separator string.
			// Syntax: arrayName.join('separatorString');

	let users = ['John', 'Jane', 'Joe', 'Robert'];

	console.log(users.join());
	console.log(users.join(" "));
	console.log(users.join(" - "));

// [SECTION] Iteration Methods
	// iteration methods are loops designed to perform repetitive tasks on arrays
	// iteration method loop over all elements in an array.

		// First Iteration Method: forEach()
			// similar to for loop that iterates on each of array element
			// for each element in the array, the function in the foreach method will be run.

		/*Syntax:
			arrayName.forEach(function(indivElement){
				statement/s
			})*/

	console.log(allTasks);

	let filteredTask = [];
	let task = allTasks.forEach(function(task){
		if(task.length > 10){
			filteredTask.push(task);
		}
	})

	console.log(filteredTask);

		// Second Iteration Method: map()
			// iterates on each element and returns new array with different values depending on the result of the function's operation
		// Syntax:
			/*
				arrayName.map(function(element){
					statement/s;
					return;
				})
			*/

	let numbers = [1, 2, 3, 4, 5];
	console.log(numbers);

	let numberMap = numbers.map(function(number){
		return number**2;
	})
	console.log(numberMap);
	console.log(task);

		// Third Iteration Method: every()
			// checks if all elements in an array meet the given condition.
			// this is useful in validating data stored in arrays especially when dealing with large amounts of data.
			// return true value if all elements meet the condition and false if otherwise
		// Syntax:
			/*arrayName.every(function(element){
				return expression/condition;
			})*/

	console.log(numbers);
	let allValid = numbers.every(function(number){
		return (number < 5);
	})
	console.log(allValid);

		// Fourth Iteration Method: some()
			// checks if at least one element in the array meets the given condition
			// returns a true value if at least one element meets the condition and false if none.

		// Syntax:
			/*
				arrayName.some(function(element){
					return expression/condition;
				})
			*/
	console.log(numbers);
	let someValid = numbers.some(function(number){
		return (number < 5);
	})
	console.log(someValid);

		// Fifth iteration method: filter()
			// return a new array that contains the elements which meets the given condition
			// return empty array if no element were found

		// Syntax:
			/*
				arrayName.filter(function(element){
					return expression/condition;
				})
			*/

	console.log(numbers);
	let filterValid = numbers.filter(function(number){
		return (number <= 3);
	})
	console.log(filterValid);

		// Sixth iteration method: includes()
			// include checks if the argument passed can be found in the array
			// it returns boolean which can be saved in a variable
				// return true if the argument is found in the array
				// return false if not found
		// Syntax:
		/*
			arrayName.includes(argument);
		*/

	let products = ["mouse", "keyboard", "laptop", "monitor"];
	let productFound1 = products.includes("mouse");
	console.log(productFound1);
	let productFound2 = products.includes("Mouse");
	console.log(productFound2);

		// Seventh Iteration Method: reduce()
			// evaluate elements from left to right and returns the reduce array.
			// array will turn into single value

		// Syntax:
		/*
			arrayName.reduce(function(accumulator, currentValue){
				return operation/expression;
			})
		*/

	console.log(numbers);

	let total = numbers.reduce(function(x, y){
		console.log("This is the value of x: " + x);
		console.log("This is the value of y: " + y);
		return x + y;
	})

	console.log(total);